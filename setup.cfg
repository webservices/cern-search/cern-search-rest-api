# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

[metadata]
name = cern-search-rest-api
version = attr: cern_search_rest_api.__version__
description = CERN Search as a Service.
long_description = file: README.md, CHANGES.md
keywords = CERN Search as a Service
license = GPLv3
author = CERN
author_email = cernsearch.support@cern.ch
platforms = any
url = http://search.cern.ch
classifiers =
    Development Status :: 5 - Production/Stable

[options]
include_package_data = True
packages = find:
python_requires = >=3.8
zip_safe = False

[options.extras_require]
tests =
    pytest
    pytest-runner>=3.0.0,<5

[options.entry_points]
console_scripts =
    cern_search_rest_api = invenio_app.cli:cli
invenio_config.module =
    cern_search_rest_api = cern_search_rest_api.config
invenio_search.mappings =
    test = cern_search_rest_api.modules.cernsearch.mappings.test
    indico = cern_search_rest_api.modules.cernsearch.mappings.indico
    webservices = cern_search_rest_api.modules.cernsearch.mappings.webservices
    edms = cern_search_rest_api.modules.cernsearch.mappings.edms
    archives = cern_search_rest_api.modules.cernsearch.mappings.archives
    asrservice = cern_search_rest_api.modules.cernsearch.mappings.asrservice
invenio_jsonschemas.schemas =
    test = cern_search_rest_api.modules.cernsearch.jsonschemas.test
    indico = cern_search_rest_api.modules.cernsearch.jsonschemas.indico
    webservices = cern_search_rest_api.modules.cernsearch.jsonschemas.webservices
    edms = cern_search_rest_api.modules.cernsearch.jsonschemas.edms
    archives = cern_search_rest_api.modules.cernsearch.jsonschemas.archives
    asrservice = cern_search_rest_api.modules.cernsearch.jsonschemas.asrservice
invenio_base.apps =
    cern-search = cern_search_rest_api.modules.cernsearch.ext:CERNSearch
invenio_base.api_apps =
    cern-search = cern_search_rest_api.modules.cernsearch.ext:CERNSearch
invenio_base.blueprints =
    health_check = cern_search_rest_api.modules.cernsearch.views:build_health_blueprint
    cern_search_rest_api = cern_search_rest_api.theme.views:blueprint
invenio_celery.tasks =
    cern-search = cern_search_rest_api.modules.cernsearch.tasks
    cern-search-indexer_tasks = cern_search_rest_api.modules.cernsearch.indexer_tasks
flask.commands =
    utils = cern_search_rest_api.modules.cernsearch.cli:utils
invenio_assets.webpack =
    cern_search_rest_api_theme = cern_search_rest_api.theme.webpack:theme

[aliases]
test = pytest

[tool:pytest]
env_files =
    # uncomment according to the environment being used
    .env
    #.poetry.env
