#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.
"""File Meta utilities."""
import mimetypes

from cern_search_rest_api.modules.cernsearch.utils import reverse_dict_list

FILE_EXT_COLLECTIONS = {
    "Document": ["doc", "docx", "odt", "pages", "rtf", "tex", "wpd", "txt"],
    "PDF": ["pdf"],
    "Sheet": ["ods", "xlsx", "xlsm", "xls", "numbers"],
    "Slides": ["ppt", "pptx", "pps", "odp", "key"],
}

FILE_EXT_DEFAULT_COLLECTION = "Other"

FILE_EXTENSION_MAP = reverse_dict_list(FILE_EXT_COLLECTIONS)

# Tika Meta keys. Read more:
# https://github.com/apache/tika/blob/2.3.0/tika-core/src/main/java/org/apache/tika/metadata/DublinCore.java
DC_AUTHOR = "dc:creator"
DC_TITLE = "dc:title"
DC_FORMAT = "dc:format"
DC_DESCRIPTION = "dc:description"
DC_SUBJECT = "dc:subject"
DC_CREATED = "dcterms:created"
# https://github.com/apache/tika/blob/2.3.0/tika-core/src/main/java/org/apache/tika/metadata
OFFICE_KEYWORDS = "meta:keywords"
PDF_KEYWORDS = "pdf:docinfo:keywords"


# Meta keys
META_CONTENT_TYPE = "content_type"
META_TITLE = "title"
META_AUTHORS = "authors"
META_CREATION = "creation_date"
META_KEYWORDS = "keywords"


def extract_metadata_from_processor(metadata):
    """Prepare metadata from processor."""
    extracted = {}

    if not metadata:
        return extracted

    if metadata.get(DC_AUTHOR):
        authors = metadata[DC_AUTHOR]
        extracted[META_AUTHORS] = authors.strip(" ") if isinstance(authors, str) else ", ".join(authors)
    if metadata.get(DC_FORMAT):
        extracted[META_CONTENT_TYPE] = mime_type_to_file_collection(metadata[DC_FORMAT])
    if metadata.get(DC_TITLE):
        extracted[META_TITLE] = metadata[DC_TITLE]

    keywords = _get_keywords(metadata)
    if keywords:
        if not isinstance(keywords, list):
            keywords = keywords.split(",")

        # strip
        keywords = [keyword.strip(" ") for keyword in keywords]
        extracted[META_KEYWORDS] = keywords
    if metadata.get(DC_CREATED):
        extracted[META_CREATION] = metadata[DC_CREATED]

    return extracted


def _get_keywords(metadata):
    """First we try office and pdf keywords to avoid dc:subject which also contains the description."""
    if metadata.get(OFFICE_KEYWORDS):
        return metadata[OFFICE_KEYWORDS]

    if metadata.get(PDF_KEYWORDS):
        return metadata[PDF_KEYWORDS]

    if metadata.get(DC_SUBJECT):
        subject = metadata[DC_SUBJECT]
        keywords = []
        if isinstance(subject, list):
            for el in subject:
                keywords.extend(el.split(","))

        return keywords


def mime_type_to_file_collection(mime_type):
    """Convert mime type to a friendly name collection."""
    if isinstance(mime_type, list):
        mime_type = mime_type[0]

    if not isinstance(mime_type, str):
        return FILE_EXT_DEFAULT_COLLECTION

    extensions = mimetypes.guess_all_extensions(mime_type.split(";")[0], strict=False)
    if not extensions:
        return FILE_EXT_DEFAULT_COLLECTION

    def strip_dot(extension):
        return extension.strip(".")

    for ext in extensions:
        collection = FILE_EXTENSION_MAP.get(strip_dot(ext))
        if collection:
            return collection

    return FILE_EXT_DEFAULT_COLLECTION
