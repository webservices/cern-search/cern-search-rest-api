#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""Handlers for customizing oauthclient endpoints."""

from __future__ import absolute_import, print_function

from flask import current_app

from cern_search_rest_api.modules.cernsearch.utils import get_user_provides


def is_egroup_admin():
    """Check if is admins egroup."""
    admin_access_groups = current_app.config["ADMIN_ACCESS_GROUPS"]
    # Allow based in the '_access' key
    user_provides = get_user_provides()
    # set.isdisjoint() is faster than set.intersection()
    admin_access_groups = admin_access_groups.split(",")
    return user_provides and not set(user_provides).isdisjoint(set(admin_access_groups))
