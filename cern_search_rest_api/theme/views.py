#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.
"""Blueprint used for loading templates."""

from flask import Blueprint

blueprint = Blueprint(
    "cern_search_rest_api",
    __name__,
    template_folder="templates",
    static_folder="static",
)
