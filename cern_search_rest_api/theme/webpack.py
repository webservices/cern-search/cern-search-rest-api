#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""JS/CSS Webpack bundles for theme."""

from invenio_assets.webpack import WebpackThemeBundle

theme = WebpackThemeBundle(
    __name__,
    "assets",
    default="bootstrap3",
    themes={
        "bootstrap3": dict(
            entry={
                "cern-search-rest-api-theme": "./scss/cern_search_rest_api/theme.scss",
            },
            dependencies={},
            aliases={},
        ),
        "semantic-ui": dict(
            entry={},
            dependencies={},
            aliases={
                "../../theme.config$": "less/cern_search_rest_api/theme.config",
            },
        ),
    },
)
