#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.
"""Citadel Search."""
from __future__ import absolute_import, print_function

__version__ = "2.0.1"

__all__ = ("__version__",)
