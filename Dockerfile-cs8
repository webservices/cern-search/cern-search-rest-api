# -*- coding: utf-8 -*-
#
# Copyright (C) 2018-2023 CERN.
#
# Invenio is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.
#
# Centos Stream 8 based on https://github.com/inveniosoftware/docker-invenio/blob/master/almalinux/Dockerfile

FROM cern/cs9-base:latest

RUN dnf upgrade --refresh -y && \
    dnf install -y \
        dnf-plugins-core \
        git \
        glibc-common \
        glibc-locale-source \
        glibc-langpack-en && \
    dnf config-manager --add-repo "http://linuxsoft.cern.ch/internal/repos/config9-stable/x86_64/os/" && \
    dnf install -y \
        https://linuxsoft.cern.ch/cern/centos/s9/CERN/x86_64/Packages/CERN-CA-certs-20220329-1.el9.cern.noarch.rpm

RUN localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN dnf install -y \
        python39 \
        python3.9-devel \
        dnf-utils \
        epel-release \
        make

RUN dnf groupinstall -y "Development Tools" && \
    dnf install -y \
        pip \
        cairo-devel \
        dejavu-sans-fonts \
        libffi-devel \
        libpq-devel \
        libxml2-devel \
        libxslt-devel \
        ImageMagick \
        openssl-devel \
        openldap-devel \
        bzip2-devel \
        xz-devel \
        sqlite-devel \
        xmlsec1-devel

# Symlink Python
RUN ln -nsf /usr/bin/python3.9 /usr/bin/python && ln -nsf /usr/bin/python3.9 /usr/bin/python3


# `python3-packaging` is installed by `yum` and it causes issues with `pip` installations
RUN yum remove python3-packaging -y
RUN pip3 install --upgrade pip pipenv wheel

# Install Node.js v16
RUN curl -fsSL https://rpm.nodesource.com/setup_16.x | bash - && \
    yum install -y nodejs

# Create working directory
ENV WORKING_DIR=/opt/invenio
ENV INVENIO_INSTANCE_PATH=${WORKING_DIR}/var/instance

# Create files mountpoints
RUN mkdir -p ${INVENIO_INSTANCE_PATH} && \
    mkdir ${INVENIO_INSTANCE_PATH}/data ${INVENIO_INSTANCE_PATH}/archive ${INVENIO_INSTANCE_PATH}/static

# copy everything inside /src
RUN mkdir -p ${WORKING_DIR}/src
WORKDIR ${WORKING_DIR}/src

# Set folder permissions
ENV INVENIO_USER_ID=1000
RUN chgrp -R 0 ${WORKING_DIR} && \
    chmod -R g=u ${WORKING_DIR}
RUN useradd invenio --uid ${INVENIO_USER_ID} --gid 0 && \
    chown -R invenio:root ${WORKING_DIR}
