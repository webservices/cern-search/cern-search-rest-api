#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.

"""Pytest configuration.

See https://pytest-invenio.readthedocs.io/ for documentation on which test
fixtures are available.
"""

from __future__ import absolute_import, print_function

import os

import pytest
from invenio_app.factory import create_api


@pytest.fixture(scope="module")
def create_app():
    """Application factory fixture."""
    return create_api


@pytest.fixture(scope="module")
def app_config(app_config):
    """Application configuration fixture."""
    # Use .env DB in integration Tests
    if "INVENIO_SQLALCHEMY_DATABASE_URI" in os.environ:
        app_config["SQLALCHEMY_DATABASE_URI"] = os.environ["INVENIO_SQLALCHEMY_DATABASE_URI"]
    if "SQLALCHEMY_ENGINE_OPTIONS" in os.environ:
        app_config["SQLALCHEMY_ENGINE_OPTIONS"] = os.environ["SQLALCHEMY_ENGINE_OPTIONS"]

    return app_config
