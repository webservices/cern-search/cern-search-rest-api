#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.
"""Instance initialisation tests."""

from copy import deepcopy
from http import HTTPStatus

import pytest
from flask import current_app
from invenio_search import current_search_client
from invenio_search.utils import build_alias_name

from cern_search_rest_api.config import RECORDS_REST_ENDPOINTS


def _compute_config(instance, default_doc):
    rest_endpoints = deepcopy(RECORDS_REST_ENDPOINTS)
    rest_endpoints["docid"]["search_index"] = instance

    return {
        "INSTANCE": instance,
        # search
        "SEARCH_MAPPINGS": instance,
        # indexer
        "INDEXER_DEFAULT_DOC_TYPE": default_doc,
        "INDEXER_DEFAULT_INDEX": f"{instance}-{default_doc}",
        # records rest
        "RECORDS_REST_ENDPOINTS": rest_endpoints,
    }


init_instance_params = [
    {
        **_compute_config("indico", "events_v1.0.0"),
        "PROCESS_FILE_META": "collection",
    },
    {
        **_compute_config("asrservice", "vtt_ttaas_v1.0.0"),
    },
    {
        **_compute_config("edms", "document_v5.0.0"),
    },
    {**_compute_config("archives", "archive_v3.0.0"), "SEARCH_INDEX_PREFIX": "cernsearch-egroups"},
]


@pytest.fixture(scope="module", params=init_instance_params, ids=["indico", "asrservice", "edms", "egroups-archives"])
def app_config(app_config, request):
    """Application configuration fixture."""
    return {**app_config, **request.param}


def test_init_instance(client, search_clear):
    """Test instance initialisation."""
    alias = build_alias_name(
        current_app.config["INDEXER_DEFAULT_INDEX"], prefix=current_app.config["SEARCH_INDEX_PREFIX"]
    )
    assert current_search_client.indices.exists(alias)

    res = client.get("/records/")

    assert res.status_code == HTTPStatus.OK

    assert res.json["hits"] == {"hits": [], "total": 0}
    assert res.json["links"] == {"self": "http://localhost/records/?size=10&page=1"}
