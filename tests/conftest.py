#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# This file is part of CERN Search.
# Copyright (C) 2018-2023 CERN.
#
# Citadel Search is free software; you can redistribute it and/or modify it
# under the terms of the MIT License; see LICENSE file for more details.
"""Pytest configuration.

See https://pytest-invenio.readthedocs.io/ for documentation on which test
fixtures are available.
"""

from __future__ import absolute_import, print_function

import logging
from unittest.mock import patch

import pytest
from flask import current_app
from invenio_accounts.models import Role, User
from invenio_app.factory import env_prefix
from invenio_config import create_config_loader
from invenio_oauth2server.models import Token

# Monkey patch Werkzeug 2.1
# Flask-Login uses the safe_str_cmp method which has been removed in Werkzeug
# 2.1. Flask-Login v0.6.0 (yet to be released at the time of writing) fixes the
# issue. Once we depend on Flask-Login v0.6.0 as the minimal version in
# Flask-Security-Invenio/Invenio-Accounts we can remove this patch again.
try:
    # Werkzeug <2.1
    from werkzeug import security

    security.safe_str_cmp
except AttributeError:
    # Werkzeug >=2.1
    import hmac

    from werkzeug import security

    security.safe_str_cmp = hmac.compare_digest


def create_tests_config_loader(**kwargs):
    """Create the test configuration loader.

    Creates tbe default configuration loader and after overwrites parameters with values from the config fixtures.
    """
    default_config_loader = create_config_loader(**kwargs)

    def _config_loader(app, **kwargs_config):
        default_config_loader(app, **kwargs_config)

        # For tests configuration passed via fixture have more priority than configuration passed via env. variables
        app.config.update(**kwargs_config)

    return _config_loader


@pytest.fixture(scope="module", autouse=True)
def config_loader():
    """Fixture to mock the configuration loader."""
    tests_config_loader = create_tests_config_loader(config=None, env_prefix=env_prefix)
    with patch("invenio_app.factory.invenio_config_loader", tests_config_loader):
        yield


@pytest.fixture()
def app(app, logger):
    """Application factory fixture."""
    yield app


@pytest.fixture()
def user(db, app):
    """File system location."""
    user = User(email="test@example.com", active=True)
    db.session.add(user)

    role = Role(name="search-admin")
    role.users.append(user)
    db.session.add(role)

    db.session.commit()

    token = Token.create_personal("test", user.id)
    db.session.commit()

    app.config["API_TOKEN"] = token.access_token
    app.config["SEARCH_USE_EGROUPS"] = True

    yield user


@pytest.fixture(scope="module")
def app_config(app_config):
    """Application configuration fixture."""
    # Missing because they're set in invenio base image:
    # More info: https://github.com/inveniosoftware/docker-invenio#environment-variables
    app_config["WORKING_DIR"] = "/opt/invenio"
    app_config["USER_ID"] = 1000
    app_config["INSTANCE_PATH"] = "/opt/invenio/var/instance"
    # for sqlite engine, remove postgres specific options
    app_config["SQLALCHEMY_ENGINE_OPTIONS"] = {}

    # https://github.com/inveniosoftware/pytest-invenio/blob/539b74de019d6ad22cdf4b567caa7df953ecaca1/pytest_invenio/fixtures.py#L227
    app_config["APP_THEME"] = ["bootstrap3"]

    # TODO: why?
    app_config["RATELIMIT_GUEST_USER"] = "200 per minute"

    return app_config


@pytest.fixture()
def logger(appctx, caplog):
    """Set logger level to debug."""
    current_app.logger.setLevel(logging.DEBUG)


@pytest.fixture(scope="module")
def instance_path():
    """Connect instance path.

    Overwrite pytest-invenio fixture to avoid setting static folder to
    `os.path.join(sys.prefix, 'var/instance/static')`
    """
    pass
